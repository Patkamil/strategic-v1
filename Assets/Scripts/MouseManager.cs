﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MouseManager : MonoBehaviour {

	public GameObject selectedObject;
	void Start () {

	}
	
	void Update () {
		Vector3 cameraPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector3 mousePos = Input.mousePosition;
		mousePos.z = -1 - cameraPos.z;

		Vector3 worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
		RaycastHit2D ray = Physics2D.Raycast(new Vector2(worldPosition.x, worldPosition.y), Vector2.zero);
		//Debug.Log("AAAAAAAAAA " + worldPosition.x + "  " + Input.mousePosition + "  " + mousey.y);
		if( ray ) {

			// The collider we hit may not be the "root" of the object
			// You can grab the most "root-est" gameobject using
			// transform.root, though if your objects are nested within
			// a larger parent GameObject (like "All Units") then this might
			// not work.  An alternative is to move up the transform.parent
			// hierarchy until you find something with a particular component.

			GameObject hitObject = ray.transform.gameObject;
			SelectObject(hitObject);
		}
		else {
			ClearSelection();
		}
	}

	void SelectObject(GameObject obj) {
		if(selectedObject != null) {
			if(obj == selectedObject)
				return;

			ClearSelection();
		}

		selectedObject = obj;

		Renderer[] rs = selectedObject.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in rs) {
			if (r.gameObject.CompareTag("MapProvince"))
			{
				
				Material m = r.material;
				m.color = new Color(0.50f,0.10f,0.40f);
				r.material = m;
			}

		}
	}

	void ClearSelection() {
		if(selectedObject == null)
			return;

		Renderer[] rs = selectedObject.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in rs) {
			Material m = r.material;
			m.color = Color.white;
			r.material = m;
		}


		selectedObject = null;
	}
}
