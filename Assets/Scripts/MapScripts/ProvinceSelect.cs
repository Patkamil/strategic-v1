﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProvinceSelect : MonoBehaviour
{
    // Start is called before the first frame update
    public Material[] materials;
    public Renderer rend;
    private int index=1;
    void Start()
    {
       // rend = GetComponent<Renderer>();
       // rend.enabled = true;
    }

    // Update is called once per frame
   /* private void OnMouseDown()
    {
        if (materials.Length == 0)
        {
            //return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            index += 1;
            if (index == materials.Length + 1)
            {
                index = 1;
            }
            print(index);
            rend.sharedMaterial = materials[index - 1];
        }
    }*/

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                Debug.Log(hit.collider.gameObject.name);
                hit.collider.attachedRigidbody.AddForce(Vector2.up);
                hit.collider.gameObject.GetComponent<Renderer>().material.color = Color.HSVToRGB(255, 255, 255);
                //this.GetComponent<Renderer>().material.color = Color.HSVToRGB (0,0,0);
               
            }
        }
    }
}
