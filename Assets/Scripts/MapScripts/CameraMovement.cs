﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float moveSpeed = 5f;
    public Rigidbody rb;
    Vector3 movement;
    float scroll;
    float faster=1;
    private float screenWidth;
    private float screenHeight;
    public float offset;
    //Vector3 depth;
    //private float zoom;
    // Update is called once per frame
    private void Start()
    {
        screenWidth = Screen.width;
        screenHeight = Screen.height;

}
    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        movement.z = Input.GetAxisRaw("Depth");
        if (Input.GetKey(KeyCode.LeftShift))
        {
            faster = 3;
        }
        else
        {
            faster = 1;
        }
        if(Input.mouseScrollDelta.y > 0)
        {
            movement = new Vector3(0, 0, 1);
        }
        if (Input.mouseScrollDelta.y < 0)
        {
            movement = new Vector3(0, 0, 1);
        }
        if (Input.mousePosition.x > screenWidth - offset){
            movement = new Vector3(1, 0, 0);
        }
        if (Input.mousePosition.x < offset)
        {
            movement = new Vector3(-1, 0, 0);
        }
        if (Input.mousePosition.y > screenHeight - offset)
        {
            movement = new Vector3(0, 1, 0);
        }
        if (Input.mousePosition.y < offset)
        {
            movement = new Vector3(0, -1, 0);
        }
    }

    
    private void FixedUpdate()
    {
        scroll = Input.GetAxis("Mouse ScrollWheel");
        rb.position = (new Vector3(Mathf.Clamp(rb.position.x, -100, 100), Mathf.Clamp(rb.position.y, -100, 100), Mathf.Clamp(rb.position.z, -100, 0)));
        HandleZoom();
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime * (rb.position.z-3)*-1/5 * faster);
        float mmm = rb.position.z;
    }
    private void HandleZoom()
    {
        if (scroll != 0)
        {
            movement = new Vector3(0, 0, scroll * moveSpeed);
        }
        scroll = 0;
    }

}
