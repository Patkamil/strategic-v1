﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom : MonoBehaviour
{

    // Start is called before the first frame update
    private Vector3 cameraFollowPosition;
    private float targetZoom;
    private float zoom = 3f;
    public Camera cm;
    [SerializeField] private float zoomLerpSpeed = 10;
 
    void Start()
    {
        cm = Camera.main;
        targetZoom = cm.orthographicSize;
    }

    // Update is called once per frame
    void Update()
    {
        //HandleZoom();
        //cameraFollowPosition.z = zoom;
        //HandleManualMovement();
        //HandleEdgeScrolling();
        float scrollData;
        scrollData = Input.GetAxis("Mouse ScrollWheel");
        targetZoom -= scrollData * zoom;

        targetZoom -= scrollData * zoom;
        zoom = Mathf.Clamp(targetZoom, 4.5f, 8f);
        cm.orthographicSize = Mathf.Lerp(cm.orthographicSize, targetZoom, Time.deltaTime * zoomLerpSpeed);
    }

    /*private void FixedUpdate()
    {
        //cameraFollowPosition.z = zoom;
        //HandleZoom();
    }
    private void HandleZoom()
    {
        float zoomChangeAmount = 1;
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            cm.fieldOfView = (cm.fieldOfView + zoomChangeAmount * Time.fixedDeltaTime);
            //zoom -= zoomChangeAmount * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            cm.fieldOfView = (cm.fieldOfView - zoomChangeAmount * Time.fixedDeltaTime);
            //zoom += zoomChangeAmount * Time.deltaTime;
        }
        //cameraFollowPosition.z = zoom;
    }*/
}
