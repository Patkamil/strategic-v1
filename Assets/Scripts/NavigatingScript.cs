﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigatingScript : MonoBehaviour
{
    [SerializeField] GameObject QuitConfirm;
    public CanvasGroup uiCanvasGroup;
    // public CanvasGroup confirmQuitCanvasGroup;
    // Start is called before the first frame update
    void Start()
    {
        QuitConfirm.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape") && !QuitConfirm.activeSelf)
        {
            QuitConfirm.SetActive(true);
            uiCanvasGroup.alpha = 0.5f;
            uiCanvasGroup.interactable = false;
            uiCanvasGroup.blocksRaycasts = false;
        }
    }

    public void NoBtn()
    {
        QuitConfirm.SetActive(false);
        uiCanvasGroup.alpha = 1;
        uiCanvasGroup.interactable = true;
        uiCanvasGroup.blocksRaycasts = true;
    }

    public void YesBtn()
    {
        SceneManager.LoadScene(0);
    }
}
